# lint_ignore=E501
import face_recognition
import cv2
import os
import numpy as np
# import mysql.connector
import base64

from datetime import date
# from dateutil.parser import parse

today = date.today()
today = today.strftime("%m/%d/%Y")

# mydb = mysql.connector.connect(
#   host="localhost",
#   user="admin",
#   password="1qaz~~!QAZ~~",
#   database="py_test"
# )

# mycursor = mydb.cursor()

K = 'known_faces'
TOLERANCE = 0.6
video_capture = cv2.VideoCapture(0)
known_fe = []
known_face_names = []
process_this_frame = True

for name in os.listdir(K):
    for filename in os.listdir(f'{K}/{name}'):
        image = face_recognition.load_image_file(f'{K}/{name}/{filename}')
        encoding = face_recognition.fe(image)[0]
        known_fe.append(encoding)
        known_face_names.append(name)


fl = []
fe = []
face_names = []

while True:
    ret, frame = video_capture.read()
    # Frame Create Hocche 1/4 sizer
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2XYZ)
    # frame = cv2.flip(frame, 1)
    # BGR color to RGB ( OpenCV BGR use kore )
    rgb_small_frame = small_frame[:, :, ::-1]
    # Alternate Video frame process kora hochee jate slow na hoy
    if process_this_frame:
        # Find all the faces and face encodings in the current frame of video
        fl = face_recognition.face_locations(rgb_small_frame)
        fe = face_recognition.face_encodings(rgb_small_frame, fl)

        face_names = []
        for f in fe:
            # See if the face is a match for the known face(s)
            matches = face_recognition.compare_faces(known_fe, f)
            name = "Unknown"
            face_distances = face_recognition.face_distance(known_fe, f)
            print(face_distances)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]
                token = name+'-'+today
                token_bytes = token.encode("ascii")
                base64_token = base64.b64encode(token_bytes)

            face_names.append(name)

    process_this_frame = not process_this_frame

    # Display the results
    for (top, right, bottom, left), name in zip(fl, face_names):
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # Draw a box around the face
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 255), 2)

        # Name below the face
        cv2.rectangle(frame, (left, bottom - 25), (right, bottom), (255, 255, 255), cv2.FILLED) # noqa
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 4), font, 0.5, (0, 0, 0), 1) # noqa

    # Display the resulting image
    cv2.imshow('Video', frame)

    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release handle to the webcam
video_capture.release()
cv2.destroyAllWindows()
