# lint_ignore=E501
import face_recognition
import os
import cv2


KNOWN_FACES_DIR = "known_faces"
TOLERANCE = 0.6
# FRAME_THICKNESS = 3
# FONT_THICKNESS = 2
# MODEL = 'cnn'  # default: 'hog', other one can be 'cnn' - CUDA accelerated (if available) deep-learning pretrained model # noqa

video = cv2.VideoCapture(0)
print("Loading known faces...")
known_faces = []
known_names = []
for name in os.listdir(KNOWN_FACES_DIR):
    for filename in os.listdir(f"{KNOWN_FACES_DIR}/{name}"):
        image = face_recognition.load_image_file(f"{KNOWN_FACES_DIR}/{name}/{filename}") # noqa
        encoding = face_recognition.face_encodings(image)[0]
        known_faces.append(encoding)
        known_names.append(name)

while True:
    ret, image = video.read()
    # Resize frame of video to 1/4 size for faster face recognition processing
    small_frame = cv2.resize(image, (0, 0), fx=0.25, fy=0.25)
    # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses) # noqa
    rgb_small_frame = small_frame[:, :, ::-1]
    locations = face_recognition.face_locations(rgb_small_frame)
    encodings = face_recognition.face_encodings(rgb_small_frame, locations)
    for face_encoding, face_location in zip(encodings, locations):
        results = face_recognition.compare_faces(known_faces, face_encoding, TOLERANCE) # noqa
        match = None
        if True in results:
            match = known_names[results.index(True)]
            top_left = (face_location[3], face_location[0])
            bottom_right = (face_location[1], face_location[2])
            color = [0, 255, 0]
            # Create frame
            cv2.rectangle(image, top_left, bottom_right, color)
            top_left = (face_location[3], face_location[2])
            bottom_right = (face_location[1], face_location[2] + 22)
            cv2.rectangle(image, top_left, bottom_right, color, cv2.FILLED)
            # Write a name
            cv2.putText(
                image,
                match,
                (face_location[3] + 10, face_location[2] + 15),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                (200, 200, 200),
            )
    cv2.imshow("Video", image)
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break
